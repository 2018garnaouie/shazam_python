# Shazam Python

Création d'une application de reconnaissance musicale pour les Coding Weeks CentraleSupélec 2018

## Geting Started

 Instructions pour la démarrage du projet pour ceux qui veulent les tourner localement
 
## Prerequisites

 Programmes et bibliothéques requises pour le fonctionnement
 
## Installation

 Tuto Installation
 
## Tests

 Explication du fonctionnement des tests automatiques
 
## Deployment

 Explication de comme démarrer le projet hors d'une ambiance local
 
## Versioning

 Petit log de registre et comment on les a fait
 
## Authors 

* **Bernardo Vieira de Miranda** 
* **Elio Garnaoui**

## Acknowledgements
 Nos reconaissances
