import Tkinter

# Functions pour chaque button
def ChansonLocalCallback():
    print("ChansonLocal")

def ShazamCallback():
    print("Shazam")

def RepertoireCallback():
    print("Repertoire")

# Creation Page Principal (ROOT)
page_principal = Tkinter.Tk() # Page principal
page_principal.title("Shazam!") # Titre de la page (au dessous)
page_principal.config(bg = "#6699ff") # Couleur de fond
page_principal.geometry("500x600") # Taille de la fenetre

# Titre
frame_titre = Tkinter.Frame(bg="#6699ff", bd=0, width=600, height=20)
frame_titre.pack(side=Tkinter.TOP)
titre_page = Tkinter.Label(frame_titre, text="Bienvenu a Shazam!", font=("AppleGothic", 50)) # Titre principal
titre_page.config(bg="#6699ff") # Couleur Titre
titre_page.pack(side=Tkinter.BOTTOM)

# Buttons
frame_button = Tkinter.Frame(bg="#6699ff", bd=0, width=600, height=30)
frame_button.pack()

# Buttons selectioner chanson local
button_local = Tkinter.Button(frame_button, text="Fichier Chanson", font=("AppleGothic", 15)) # Creation Button
button_local.config(highlightbackgroun="#6699ff", relief=Tkinter.GROOVE, command=ChansonLocalCallback)
button_local.pack(side=Tkinter.LEFT)
space_label_local = Tkinter.Label(frame_button,bg="#6699ff",width=4)
space_label_local.pack(side=Tkinter.LEFT)

# Buttons micro
button_micro = Tkinter.Button(frame_button, text="Shazam!", font=("AppleGothic", 15)) # Creation Button
button_micro.config(highlightbackgroun="#6699ff", relief=Tkinter.GROOVE, command=ShazamCallback)
button_micro.pack(side=Tkinter.LEFT)
space_label_micro = Tkinter.Label(frame_button,bg="#6699ff",width=4)
space_label_micro.pack(side=Tkinter.LEFT)

# Buttons mes chansons
button_chanson = Tkinter.Button(frame_button, text="Mes Chansons", font=("AppleGothic", 15)) # Creation Button
button_chanson.config(highlightbackgroun="#6699ff", relief=Tkinter.GROOVE, command=RepertoireCallback)
button_chanson.pack(side=Tkinter.LEFT)
space_label_chanson = Tkinter.Label(frame_button,bg="#6699ff",width=4)
space_label_chanson.pack(side=Tkinter.LEFT)

page_principal.mainloop() # Montrer page
